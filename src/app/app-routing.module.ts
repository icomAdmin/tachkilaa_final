import { NgModule } from '@angular/core'; 
import { RouterModule, Routes } from '@angular/router';  
import { AppMaterialModule } from './app-matrial.module';
import { E404Component } from './main/pages/erreur/e404/e404.component';
import { E500Component } from './main/pages/erreur/e500/e500.component';
import { ForgotPasswordComponent } from './main/pages/authotification/forgot-password/forgot-password.component';
import { LoginComponent } from './main/pages/authotification/login/login.component';
import { InscriptionComponent } from './main/pages/authotification/inscription/inscription.component';
import { DashbordComponent } from './main/dashbord/dashbord.component';
import { AddTerrainComponent } from './main/terrains/add-terrain/add-terrain.component';
import { ListeTerrainComponent } from './main/terrains/liste-terrain/liste-terrain.component';
import { HandBallComponent } from './main/type-terrain/hand-ball/hand-ball.component';
import { BasketBallComponent } from './main/type-terrain/basket-ball/basket-ball.component';
import { FootBallComponent } from './main/type-terrain/foot-ball/foot-ball.component';
import { VolleyBallComponent } from './main/type-terrain/volley-ball/volley-ball.component';
import { TennisComponent } from './main/type-terrain/tennis/tennis.component';
import { AutreComponent } from './main/type-terrain/autre/autre.component';
import { CommonModule, DatePipe } from '@angular/common';
import { CourseComponent } from './main/type-terrain/course/course.component';
import { DetailTerrainComponent } from './main/type-terrain/detail-terrain/detail-terrain.component';
import { TerrainDataService } from './services/Data Service/terrain.service';
import { HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms';
import { AddMatchComponent } from './main/matchs/add-match/add-match.component';
import { ListeMatchComponent } from './main/matchs/liste-match/liste-match.component';
import { MatConfirmDialogComponent } from './main/mat-confirm-dialog/mat-confirm-dialog.component';
import { ChatService } from './services/Data Service/chat.service';

import { ProfilComponent } from './main/profil/profil.component';
import { AgmCoreModule } from '@agm/core';
import { RechercheMapComponent } from './main/dashbord/recherche-map/recherche-map.component';

import { OwlModule } from 'ngx-owl-carousel';
import { MailConfirmComponent } from './main/pages/authotification/mail-confirm/mail-confirm.component';
import { ResetPasswordComponent } from './main/pages/authotification/reset-password/reset-password.component';
import { AuthGuard } from './services/guards/auth.guard';
import { ValidatedGuard } from './services/guards/validated.guard';
import { NotAuthGuard } from './services/guards/not-auth.guard';
import { NotValidatedGuard } from './services/guards/not-validated.guard';
import { TypeUserComponent } from './main/type-user/type-user.component';
import { EquipesComponent } from './main/equipes/equipes.component';
import { AddEquipeComponent } from './main/equipes/add-equipe/add-equipe.component';
import { DetailEquipeComponent } from './main/equipes/detail-equipe/detail-equipe.component';
import { ServerResolver } from './models/equipe';
import { NotificationService } from './services/Data Service/notification.service';
import { AnalyticsDashboardModule } from './main/Acceuil/acceuil.module';
import { UsersComponent } from './main/users/users.component';
import { AddUsersComponent } from './main/users/add-users/add-users.component';
import { UserService } from './services/Data Service/user.service';
const appRoutes: Routes =[
       {path: 'apps',loadChildren: './main/apps/apps.module#AppsModule'},       
       {path:'dashbord',component:DashbordComponent,canActivate:[ValidatedGuard]},
       {path:'equipe',component:EquipesComponent,canActivate:[ValidatedGuard],
       children: [
       { path: ':id', component: DetailEquipeComponent, resolve: {server: ServerResolver}},
     ]
    },
       {path:'typeuser',component:TypeUserComponent,canActivate:[AuthGuard]},
       {path:'listeterrain',component:ListeTerrainComponent,canActivate:[ValidatedGuard]},
       {path:'addmatch',component:AddMatchComponent,canActivate:[ValidatedGuard]},
       {path:'listematch',component:ListeMatchComponent,canActivate:[ValidatedGuard]},
       {path:'foot',component:FootBallComponent,canActivate:[ValidatedGuard]},
       {path:'foot/:id', component:DetailTerrainComponent,canActivate:[ValidatedGuard]},
       { path: 'mail-confirm', component: MailConfirmComponent},
       {path:'basket',component:BasketBallComponent,canActivate:[ValidatedGuard]},
       {path:'hand',component:HandBallComponent,canActivate:[ValidatedGuard]},
       {path:'volley',component:VolleyBallComponent,canActivate:[ValidatedGuard]},    
       {path:'inscri',component:InscriptionComponent},    
       {path:'tennis',component:TennisComponent,canActivate:[ValidatedGuard]}, 
       {path:'course',component:CourseComponent,canActivate:[ValidatedGuard]},    
       {path:'404',component:E404Component},    
       {path:'login',component:LoginComponent}, 
       {path:'inscription',component:InscriptionComponent}, 
       {path:'forgotpassword',component:ForgotPasswordComponent},
       {path:'detailterrain',component:DetailTerrainComponent,canActivate:[ValidatedGuard]},
       {path:'addterrain',component:AddTerrainComponent,canActivate:[ValidatedGuard]},
       {path:'rech',component:RechercheMapComponent,canActivate:[ValidatedGuard]},
       {path: 'auth/reset-password', component: ResetPasswordComponent },
       {path:'profil',component:ProfilComponent,canActivate:[ValidatedGuard]},
       {path:'user',component:UsersComponent,canActivate:[ValidatedGuard]},
       {path: '**',redirectTo: '404' }
       
     
     
       ];
       
@NgModule({
    declarations: [  
        DashbordComponent,
        AddTerrainComponent,
        ListeTerrainComponent,
        E404Component,
        E500Component,
        ForgotPasswordComponent,
        InscriptionComponent,
        LoginComponent,
        FootBallComponent,
        BasketBallComponent,
        HandBallComponent,
        VolleyBallComponent,
        TennisComponent,
        AutreComponent,
        CourseComponent,
        DetailTerrainComponent,
        // DropZoneDirective,
        AddMatchComponent,
        ListeMatchComponent,
        MatConfirmDialogComponent,
        ProfilComponent,
        RechercheMapComponent,
        MailConfirmComponent,
        ResetPasswordComponent,
        TypeUserComponent,
        EquipesComponent,
        AddEquipeComponent,
        DetailEquipeComponent,
        UsersComponent,
        AddUsersComponent,
],
imports     : [ 
        RouterModule.forRoot(appRoutes, {useHash: true }),
        AppMaterialModule,
        CommonModule,
        HttpModule,
        AgmCoreModule.forRoot({
            apiKey:'AIzaSyDZt_9RcYvqfLVpUptCOK3ju_iFlnSv8IE',
            libraries: ["places"]
        }),
        AgmCoreModule,
        FormsModule,
        OwlModule,
        AnalyticsDashboardModule,
    ],
 providers: [
        TerrainDataService,
        ChatService,
         DatePipe,
         AuthGuard,
         ValidatedGuard,
         NotAuthGuard,
         UserService,
         NotValidatedGuard,
         ServerResolver,
         NotificationService,
    ],
 entryComponents:[
        AddMatchComponent,
        MatConfirmDialogComponent,
        AddTerrainComponent,
        AddEquipeComponent,
        AddUsersComponent
    ]
})
export class AppRoutingModule
{
}
