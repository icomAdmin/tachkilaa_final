import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';


@Component({
    selector     : 'analytics-dashboard',
    templateUrl  : './acceuil.component.html',
    styleUrls    : ['./acceuil.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AnalyticsDashboardComponent implements OnInit
{
   
    colorScheme = {
        domain: [
          '#C0C0C0', 
          '#ffffff',
        ]
      };
        single = [
        {
          "name": "inscri",
          "value": 8940000
        },
        {
          "name": "non inscri",
          "value": 5000000
        }
      ];
    
    constructor(
        private _fuseConfigService: FuseConfigService,
    )
    {
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

   
    ngOnInit(): void
    {

    }

   
}

