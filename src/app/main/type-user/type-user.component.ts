import { Component, OnInit } from '@angular/core';
import { FuseConfigService } from '@fuse/services/config.service';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/Data Service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-type-user',
  templateUrl: './type-user.component.html',
  styleUrls: ['./type-user.component.scss']
})
export class TypeUserComponent implements OnInit {
  user:User
  constructor(        private _fuseConfigService: FuseConfigService,
    public auth: AuthService, private router: Router
    ) {
      this._fuseConfigService.config = {
        layout: {
            navbar   : {
                hidden: true
            },
            toolbar  : {
                hidden: true
            },
            footer   : {
                hidden: true
            },
            sidepanel: {
                hidden: true
            }
        }
    };
   }

  ngOnInit() {
  }
  async updateTypeUser(type)
  {
     if(type=="client")
  {
   await  this.auth.updateUsertype(type);
    this.router.navigate(['/dashbord']);
  }else {
    await  this.auth.updateUsertype(type);
    this.router.navigate(['/dashbord']);
  }

  }
}
