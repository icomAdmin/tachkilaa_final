import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { Subject, Subscription } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';
import { Terrain } from 'app/models/terrain';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { ActivatedRoute, Params, Router } from '@angular/router';


@Component({
  selector: 'app-detail-terrain',
  templateUrl: './detail-terrain.component.html',
  styleUrls: ['./detail-terrain.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailTerrainComponent implements OnInit

{
  terrains: Terrain;
  terrain:any[];
  id: string;  
  title: string = 'My first AGM project';
  lat: number ;
  lng: number ;
  paramsSubscription : Subscription;
static getid :string;

    constructor(
        public terraindataservice:TerrainDataService,
        private route:ActivatedRoute,
        private router: Router,
    )
    {
      this.paramsSubscription = this.route.params.subscribe(
        (params : Params)=> {
          this.id= this.route.snapshot.params['id'];    
          DetailTerrainComponent.getid=this.route.snapshot.params['id']; 
  
        }
      );
        
    }

   
    ngOnInit(): void
    {
      this.getDetail();


    }


    getDetail(){
      this.terraindataservice.getTerrain(this.id).subscribe( res =>{
        
        if (res){
             this.terrains = res ; 
             this.lat= parseFloat(this.terrains.latitude);
             this.lng= parseFloat(this.terrains.longitude);
        }
      }   );
  
      }
    
      onCreate(){
       this.router.navigate(['/apps/calendar',this.id]);

      }
  
}


