import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AddTerrainComponent } from 'app/main/terrains/add-terrain/add-terrain.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { Terrain } from 'app/models/terrain';
import { AuthService } from 'app/services/Data Service/auth.service';
import { DialogService } from 'app/services/Data Service/dialog.service';
import { NotificationService } from 'app/services/Data Service/notification.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'], 
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class CourseComponent implements OnInit {

  terrain:Terrain[];
  terrain2:any[];
  terrain3:any[];
  currentterrain: string;
  terrainfilter:Terrain[]=[];
  terrainfilter2:Terrain[]=[];
  searchTerm: string;
type:boolean;
 dialogRef: any; 
 userco:string;

 // Private  
 private _unsubscribeAll: Subject<any>;

 /**
  * Constructor
  *
  * @param {MatDialog} _matDialog
  */
  constructor(private _matDialog: MatDialog,
    public service:TerrainDataService,  
    private dialog: MatDialog, 
    public auth:AuthService,
    private dialogService: DialogService,
    public notificationService: NotificationService,
    )
  {
    this.type=false;
    this.searchTerm = '';
    
    if(this.service)
    {    this.currentterrain =this.service.type ;
      this.service.type="all";}
    else{this.currentterrain = 'all';}
    
   
    this._unsubscribeAll = new Subject();
    this.service.getterrains().subscribe(terrain => {
      this.terrain = terrain.map(item => {
       
        return {
          $key: item.key,
          ...item.payload.val()
        }; 
      });
      for (let index of this.terrain) {
        if(index.typeterrain==="Course"){
       const element = index;
       this.terrainfilter.push(element);
 
     }}
     this.terrainfilter2=this.terrainfilter;
         this.terrain2=this.terrainfilter;
         this.terrain3=this.terrainfilter;
        this.filterCoursesByCategory();
        this.terrainfilter = [];

  });  

}
  onCreate() {
    this.service.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(AddTerrainComponent,dialogConfig);
  }
  ngOnInit() { 
    this.auth.user$.subscribe((res)=>
    { this.userco=res.email; 
if( res.type === "prestataire") 
       this.type=true;
else 
      this.type=false;

   });
  

  }
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }
  filterCoursesByTerm(): void
    {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if ( searchTerm === '' )
        { 
            this.terrainfilter2 = this.terrain2;
        }
        else
        {
            this.terrainfilter2 = this.terrain2.filter((terrain) => {
                return terrain.name.toLowerCase().includes(searchTerm);
            });
        }
    }
  
    filterCoursesByCategory(): void
    {
        // Filter
        if ( this.currentterrain === 'all' )
        {
            this.terrain2 = this.terrain3;
            this.terrainfilter2 = this.terrain3;
        }
        else
        {
            this.terrain2 = this.terrain3.filter((terrain) => {
                return terrain.city === this.currentterrain;
            });

            this.terrainfilter2 = [...this.terrain2];
 
        }

        this.filterCoursesByTerm();
    }
    onEdit(row){
      this.service.populateForm(row);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = "50%";
      this.dialog.open(AddTerrainComponent,dialogConfig);
    }
    onDelete($key){
      this.dialogService.openConfirmDialog('voulez vous supprimer ce terrain ?')
      .afterClosed().subscribe(res =>{
        if(res){
          this.service.deleteterrain($key);
          this.notificationService.warn('Deleted successfully');
        }
      });
    }
}