import { Component, OnInit,ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource,MatSort,MatPaginator, MatDialogConfig, MatDialog } from '@angular/material';
import { MatchDataService } from 'app/services/Data Service/match.service';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { AddMatchComponent } from '../add-match/add-match.component';
import { DialogService } from 'app/services/Data Service/dialog.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/services/Data Service/auth.service';
import { Match } from 'app/models/match';
import { startOfDay } from 'date-fns';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'app/services/Data Service/notification.service';

@Component({
  selector: 'app-liste-match',
  templateUrl: './liste-match.component.html',
  styleUrls: ['./liste-match.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListeMatchComponent implements OnInit {
  match:any[];
  user:string;
  matchfiltrer:Match[]=[];
  today: number = Date.now();
  today2:any;
  constructor(private service: MatchDataService,
    private terrainDataService: TerrainDataService,
     private dialog: MatDialog,
     public auth:AuthService,
     private datePipe: DatePipe,
     private dialogService: DialogService,
     public notificationService: NotificationService,
    ) {        
      
this.today2= datePipe.transform(this.today,'yyyy-MM-dd');
  }
 
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['user','Jour', 'Time','terrainname','actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string; 
  
  ngOnInit() {
this.filtrer();
  }
  filtrer(){

    this.service.getmatchs().subscribe(list => {      
      this.match = list.map(item => { 
        let terrainname = this.terrainDataService.getTerrainName(item.payload.val()['terrain']);
          return {
            $key: item.key,
            terrainname,
            ...item.payload.val() }; 
           });
           this.auth.user$.subscribe((res)=>
           {this.user=res.displayName;   
  
           for (let index of this.match) {
             if(index.capitainename===this.user && index.Datedeb>=this.today2){
           this.matchfiltrer.push(index); 
           
            }}
          this.listData = new MatTableDataSource(this.matchfiltrer);
          this.matchfiltrer = [];
          this.listData.sort = this.sort;
          this.listData.paginator = this.paginator;
               

          });
          
      });
     
  }
  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  onEdit(row){
    this.service.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "30%";
    this.dialog.open(AddMatchComponent,dialogConfig);
  }
  onDelete($key){
    this.dialogService.openConfirmDialog('voulez vous supprimer ce match ?')
    .afterClosed().subscribe(res =>{
      if(res){
        this.service.deletematch($key);
        this.notificationService.warn('Deleted successfully');
      }
    });
  }
}
