import { Component, OnInit } from '@angular/core';
import { MatchDataService } from 'app/services/Data Service/match.service';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { MatDialogRef } from '@angular/material';
import { AuthService } from 'app/services/Data Service/auth.service';
import { NotificationService } from 'app/services/Data Service/notification.service';
import { DialogService } from 'app/services/Data Service/dialog.service';
@Component({
  selector: 'app-add-match',
  templateUrl: './add-match.component.html',
  styleUrls: ['./add-match.component.scss']
})
export class AddMatchComponent implements OnInit {
terrain:string;
name:string;
tof:string;
tel:string;
durre:string;
today: number = Date.now();

  constructor(private service: MatchDataService,
     private terrainDataService: TerrainDataService,
     public dialogRef: MatDialogRef<AddMatchComponent>,
     private notificationService: NotificationService,
     public auth:AuthService,
     public dialogService: DialogService,
    ) {



  }
  ngOnInit() {
    this.auth.user$.subscribe((res)=>
    {
      this.name = res.displayName;
      if (res.photoURL === null){
        this.tof ="https://img.icons8.com/color/100/000000/gender-neutral-user.png";
    }
    else this.tof= res.photoURL;   
   });
  this.terrain=this.terrainDataService.currentterrain;
this.tel='216'+this.terrainDataService.getTerraincontact(this.terrain);
console.log(this.tel);

   this.service.getmatchs();

  }
  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.onClose();

  }
  onSubmit() {
    if (this.service.form.valid) {
      if (!this.service.form.get('$key').value)

{
  
  this.service.form.patchValue({terrain: this.terrain});
  this.service.form.patchValue({capitainename: this.name});
  this.service.form.patchValue({capitainetof: this.tof});
  this.service.insertmatch(this.service.form.value);

}      else
      this.service.updatematch(this.service.form.value);
      this.service.form.reset();
      this.service.initializeFormGroup();
      this.notificationService.success('successfully');
      this.onClose();
    } 
  }

  onDelete($key: string) {
    this.dialogService.openConfirmDialog('Are you sure to delete this record ?')
      .afterClosed().subscribe(res => {
        if (res) {
          this.service.deletematch($key);
          this.service.form.reset();
          this.service.initializeFormGroup();
          this.notificationService.warn('Deleted successfully');
          this.onClose();
        }
      });

  }

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
  }
}
