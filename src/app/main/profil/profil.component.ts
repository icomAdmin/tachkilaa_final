import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from 'app/services/Data Service/auth.service';
import { DatePipe } from '@angular/common';
import { Match } from 'app/models/match';
import { MatchDataService } from 'app/services/Data Service/match.service';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { EquipeService } from 'app/services/Data Service/equipe.service';
import { Equipe } from 'app/models/equipe';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfilComponent implements OnInit {
nameuser:string;
image:string;
equip:Equipe[];
datecreation:DatePipe;
email:string;
dtc:DatePipe;
type:string;
match:any[];
  user:string;
  matchfiltrer2:Match[]=[];
  matchfiltrer:Match[]=[];
  today: number = Date.now();
  today2:any;
  match2:any[];
  equipefilter:Equipe[]=[];
  constructor(public auth:AuthService,private service: MatchDataService,private servicequipe: EquipeService
    ,private datePipe: DatePipe,     private terrainDataService: TerrainDataService,

    ) { }

  ngOnInit() {
    this.auth.user$.subscribe((res)=>
    {
      this.nameuser = res.displayName;
this.datecreation=res.creationTime;
this.email=res.email;
this.dtc=res.lastSignInTime;
this.type=res.type;
      if (res.photoURL === null){
        this.image ="https://img.icons8.com/color/100/000000/gender-neutral-user.png";
    }
    else this.image= res.photoURL;
    });
    this.historique();
    this.rapelle();
    this.equipe();
  }
  historique(){
    this.today2= this.datePipe.transform(this.today,'yyyy-MM-dd');
    this.service.getmatchs().subscribe(list => {      
      this.match = list.map(item => { 
        let terrainname = this.terrainDataService.getTerrainName(item.payload.val()['terrain']);
          return {
            $key: item.key,
            terrainname,
            ...item.payload.val() }; 
           });
           this.auth.user$.subscribe((res)=>
           {this.user=res.displayName;   
 
           for (let index of this.match) {
             if(index.capitainename===this.user && index.Datedeb <= this.today2){
           this.matchfiltrer.push(index); 
            }}

               

          });
          
      });
  }
  rapelle(){
    this.today2= this.datePipe.transform(this.today,'yyyy-MM-dd');
    this.service.getmatchs().subscribe(list => {      
      this.match = list.map(item => { 
          return {
            $key: item.key,
            ...item.payload.val() }; 
           });
           this.auth.user$.subscribe((res)=>
           {this.user=res.displayName;   
 
           for (let index of this.match) {
             if(index.capitainename===this.user && index.Datedeb>=this.today2){
           this.matchfiltrer2.push(index); 
            }}
          });
          
      });
  }
  equipe(){
    this.today2= this.datePipe.transform(this.today,'yyyy-MM-dd');
   this.servicequipe.getequipes().subscribe(equipe => {
     this.equip = equipe.map(item => {
      
       return {
         $key: item.key,
         ...item.payload.val()
       }; 
     });
    this.auth.user$.subscribe((res)=>
           {this.user=res.displayName;   
 
           for (let index of this.equip) {
             if(index.capitainename===this.user && index.Datedeb <= this.today2){
           this.equipefilter.push(index); 
            }}
          });
  });
  }
    async updateTypeUser(type)
    {
       if(type=="client")
    {
     await  this.auth.updateUsertype(type);
    }else {
      await  this.auth.updateUsertype(type);
    }
  
    }
  
}
