import { Component, OnInit,OnDestroy, Inject, ViewEncapsulation } from '@angular/core';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { Terrain } from 'app/models/terrain';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from '@angular/fire/firestore';
import { finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListeTerrainComponent } from '../liste-terrain/liste-terrain.component';
import { AuthService } from 'app/services/Data Service/auth.service';
import { NotificationService } from 'app/services/Data Service/notification.service';
@Component({
  selector: 'app-add-terrain',
  templateUrl: './add-terrain.component.html',
  styleUrls: ['./add-terrain.component.scss']
})
export class AddTerrainComponent implements OnInit {

// mtaa toof
 // Main task 
 task: AngularFireUploadTask;

 // Progress monitoring
 percentage: Observable<number>;

 snapshot: Observable<any>;

 // Download URL
 downloadURL: Observable<string>;

 // State for dropzone CSS toggling
 isHovering: boolean;
email:string;
 
  constructor(public service:TerrainDataService,
    private storage: AngularFireStorage, 
     public dialogRef: MatDialogRef<AddTerrainComponent>,
     public auth:AuthService,
     private notificationService: NotificationService,
    ) {
    }
    ngOnInit() {
      this.auth.user$.subscribe((res)=>
    {
      this.email = res.email;
      
   });
      this.service.getterrains();
    }
  
    onClear() {
      this.service.form.reset();
      this.service.initializeFormGroup();
    }
  
    onSubmit() {
      if (this.service.form.valid) {
        if (!this.service.form.get('$key').value){
          this.service.form.patchValue({usermail: this.email});
          this.service.insertterrain(this.service.form.value);
          this.notificationService.success('Submitted successfully');
        }
        else
        this.service.updateterrain(this.service.form.value);
        this.service.form.reset(); 
        this.service.initializeFormGroup();
        this.onClose();
      }
    }
  
    onClose() {
      this.service.form.reset();
      this.service.initializeFormGroup();
      this.dialogRef.close();
    }
//mtaa tooof
toggleHover(event: boolean) {
  this.isHovering = event;
}

startUpload(event: FileList) {
  // The File object
  const file = event.item(0)

  // Client-side validation example
  if (file.type.split('/')[0] !== 'image') { 
    console.log(file);
    
    console.error('unsupported file type :( ')
    return;
  }

  // The storage path
  const path = `test/${new Date().getTime()}_${file.name}`;

  // Totally optional metadata
  const customMetadata = { app: 'My AngularFire-powered PWA!' };

  // The main task
  this.task = this.storage.upload(path, file, { customMetadata })

  // Progress monitoring
  this.percentage = this.task.percentageChanges();
  this.snapshot   = this.task.snapshotChanges()

  this.snapshot.pipe(finalize(() => this.downloadURL = this.storage.ref(path).getDownloadURL())).subscribe(); 
 
  
   

}

isActive(snapshot) {
  return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
}

 
}
