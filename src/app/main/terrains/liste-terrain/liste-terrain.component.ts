import { Component, OnInit } from '@angular/core';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';
import { Terrain } from 'app/models/terrain';

@Component({
  selector: 'app-liste-terrain',
  templateUrl: './liste-terrain.component.html',
  styleUrls: ['./liste-terrain.component.scss']
})
export class ListeTerrainComponent implements OnInit {
  terrain:Terrain[];
  terrainfilter:Terrain[]=[];
foot:number=0;
hand:number=0;
tennis:number=0;
basket:number=0;
volly:number=0;
tenis:number=0;
course:number=0;
rug:number=0;
badm:number=0;

constructor(    public service:TerrainDataService,  
    ) { }

  ngOnInit() {
    this.service.getterrains().subscribe(terrain => {
      this.terrain = terrain.map(item => {
       
        return {
          $key: item.key,
          ...item.payload.val()
        }; 
      });
      for (let index of this.terrain) {
     switch(index.typeterrain) { 
      case "Foot-ball": { 
        this.terrainfilter.push(index);
        this.foot = this.terrainfilter.length;
                 break; 
      } 
      case "Basket-ball": { 
        this.terrainfilter.push(index);
        this.basket = this.terrainfilter.length;
                 break; 
      } 
      case "Hand-ball": { 
        this.terrainfilter.push(index);
        this.hand = this.terrainfilter.length;
                 break; 
      }
      case "Volly-ball": { 
        this.terrainfilter.push(index);
        this.volly = this.terrainfilter.length;
                 break; 
      }
      case "Tennis": { 
        this.terrainfilter.push(index);
        this.tenis = this.terrainfilter.length;
                 break; 
      }
      case "Course": { 
        this.terrainfilter.push(index);
        this.course = this.terrainfilter.length;
                 break; 
      }
      case "Badminton": { 
        this.terrainfilter.push(index);
        this.badm = this.terrainfilter.length;
                 break; 
      }
      case "Rugby": { 
        this.terrainfilter.push(index);
        this.rug = this.terrainfilter.length;
                 break; 
      }
   } 
    }
         
  });  
  }

}
