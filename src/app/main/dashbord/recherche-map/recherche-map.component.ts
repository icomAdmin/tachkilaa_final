import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';

@Component({
  selector: 'app-recherche-map',
  templateUrl: './recherche-map.component.html',
  styleUrls: ['./recherche-map.component.scss']
})
export class RechercheMapComponent implements OnInit {
  latitude: number;
  longitude: number ;
  zoom :number;
  marker :any[];
  public searchControl: FormControl;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  mapAddress:any;
  mapAllo : any;

  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private terrainDataService: TerrainDataService,

    ) { } 
  ngOnInit() {
    this.zoom = 10;
    this.latitude = 36.8189700;
    this.longitude = 10.1657900;

    this.searchControl = new FormControl();
    this.setCurrentPosition();
    this.mapsAPILoader.load().then( res => {
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      types: [],
    });
    
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.mapAllo = place;
        this.mapAddress = place.formatted_address;
        if (place.geometry === undefined || place.geometry === null) {
          return;
        } 
        this.latitude = place.geometry.location.lat();
        this.longitude = place.geometry.location.lng();
        this.zoom = 12;
         
      });
    });
  });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        
      });
    }
  }

  
}
