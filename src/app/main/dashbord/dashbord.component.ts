import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
// import { FootBallComponent } from '../type-terrain/foot-ball/foot-ball.component';
import { TerrainDataService } from 'app/services/Data Service/terrain.service';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class DashbordComponent implements OnInit {
  date:Date;
  city:string;
  categorie:string;
  view: string;
  showmap:boolean=false;
  lat: number = 51.678418;
  lng: number = 7.809007;
  form: FormGroup = new FormGroup({
    date: new FormControl(''),
    city: new FormControl(0),
    categorie: new FormControl(''),
  });
  constructor(private router: Router, public terrainDataService:TerrainDataService,
    ) {        this.view = 'preview';
  }

  ngOnInit() {
  }
  cherche(){
   this.date=this.form.value.date;
  
  switch(this.form.value.categorie) { 
    case 'Foot-ball': { 
      this.router.navigate(['/foot']);
       this.terrainDataService.type=this.form.value.city;
       
      break; 
    } 
    case 'Hand-ball': { 
      this.router.navigate(['/hand']);
      this.terrainDataService.type=this.form.value.city;

      break; 
    } 
    case 'Tennis': { 
      this.router.navigate(['/tennis']);
      this.terrainDataService.type=this.form.value.city;

      break; 
    } 
    case 'Basket-ball': { 
      this.router.navigate(['/basket']);
      this.terrainDataService.type=this.form.value.city;

      break; 
    } 
    case 'Volly-ball': { 
      this.router.navigate(['/volley']);
      this.terrainDataService.type=this.form.value.city;

      break; 
    } 
    case 'Course': { 
      this.router.navigate(['/course']);
      this.terrainDataService.type=this.form.value.city;

      break; 
    } 
    
  }
}
toggleView(){
  console.log(this.showmap)
 this.showmap=!this.showmap; 
}
}

