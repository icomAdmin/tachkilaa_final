import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-e404',
  templateUrl: './e404.component.html',
  styleUrls: ['./e404.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class E404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
