import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { UserService } from 'app/services/Data Service/user.service';
import { NotificationService } from 'app/services/Data Service/notification.service';
import { MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DialogService } from 'app/services/Data Service/dialog.service';
import { AddUsersComponent } from './add-users/add-users.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class UsersComponent implements OnInit {
  constructor(public userService: UserService,
    public notificationService: NotificationService,
    private dialog: MatDialog,
    public dialogService: DialogService) { }

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['displayName', 'email', 'emailVerified', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string = "";
  ngOnInit() {

    this.userService.getUsers().subscribe(
      list => {
        let array = list.map(item => {

          return {
          
            ...item
          };


        });
       
        this.listData = new MatTableDataSource(array);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions'&& ele != 'emailVerified' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
      });

  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  onCreate() {
    this.userService.initializeFormGroup();
    this.dialog.open(AddUsersComponent, { panelClass: 'contact-form-dialog' });
  }
  onEdit(row) {
  
    const data={uid:row.uid,displayName:row.displayName,email:row.email
      ,emailVerified:row.emailVerified,photoURL:row.photoURL};
     
    this.userService.populateForm(data);
    this.dialog.open(AddUsersComponent, { panelClass: 'contact-form-dialog' });
  }

  onDelete($key) {

    this.dialogService.openConfirmDialog('Are you sure to delete this record ?')
      .afterClosed().subscribe(res => {
        if (res) {
          this.userService.deleteUser($key);
          this.notificationService.warn('Deleted successfully');
        }
      });
  }
}
