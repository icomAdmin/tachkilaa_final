import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/services/Data Service/auth.service';
import { MatDialogRef } from '@angular/material';
import { DialogService } from 'app/services/Data Service/dialog.service';
import { UserService } from 'app/services/Data Service/user.service';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class AddUsersComponent implements OnInit {

 
  constructor(public userService: UserService,
    public dialogRef: MatDialogRef<AddUsersComponent>,
    public dialogService: DialogService,
    public authService:AuthService) { }

 
    ngOnInit() {
      this.userService.getUsers();
    }
    onSubmit() {
    
      if (this.userService.form.valid) {
         this.userService.insertUser(this.userService.form.value);
        this.userService.form.reset();
  
        this.userService.initializeFormGroup();
        this.onClose();
      }
    }
    onUpdate() {
      if (this.userService.form.valid) {
        this.userService.updateUser(this.userService.form.value);
        this.userService.form.reset();
        this.userService.initializeFormGroup();
        this.onClose();
      }
    }
    onDelete($key: string) {
      this.dialogService.openConfirmDialog('Are you sure to delete this record ?')
        .afterClosed().subscribe(res => {
          if (res) {
            this.userService.deleteUser($key);
            this.userService.form.reset();
            this.userService.initializeFormGroup();
            this.onClose();
          }
        });
  
    }
    onClose() {
      this.userService.form.reset();
      this.userService.initializeFormGroup();
      this.dialogRef.close();
    }
  
    ngOnDestroy() {
      this.userService.form.reset();
  
      this.userService.initializeFormGroup();
    }  

}
