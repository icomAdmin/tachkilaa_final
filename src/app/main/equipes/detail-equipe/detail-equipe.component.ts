import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, Data, Params } from '@angular/router';
import { Equipe } from 'app/models/equipe';
import { EquipeService } from 'app/services/Data Service/equipe.service';
import { fuseAnimations } from '@fuse/animations';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-detail-equipe',
  templateUrl: './detail-equipe.component.html',
  styleUrls: ['./detail-equipe.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailEquipeComponent implements OnInit {
  equipe:Equipe;
  paramsSubscription: Subscription;
  id:string;
  showDetails: boolean;
  constructor( private route: ActivatedRoute, private equipeservice :EquipeService, 
) { 
  this.paramsSubscription = this.route.params.subscribe(
    (params : Params)=> {
      this.id= this.route.snapshot.params['id'];         
    }
  );


     const id = +this.route.snapshot.params['id'];
    
      
     this.showDetails = false;
  
}

  ngOnInit() {
    this.equipeservice.getequipe(this.id).subscribe( res =>{
      if (res){  
           this.equipe = res ;         
    
              }
    });
    this.route.params.subscribe(
        (params: Params) => {
          this.equipeservice.getequipe(this.id).subscribe( res =>{
            if (res){  
                 this.equipe = res ;         
            }
          });
        }
    );
           console.log(this.equipe);
    
  }

}
