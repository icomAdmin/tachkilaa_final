import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { EquipeService } from 'app/services/Data Service/equipe.service';
import { AddEquipeComponent } from './add-equipe/add-equipe.component';
import { fuseAnimations } from '@fuse/animations';
import { Equipe } from 'app/models/equipe';
import { ActivatedRoute, Router } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { AuthService } from 'app/services/Data Service/auth.service';
import { DialogService } from 'app/services/Data Service/dialog.service';
import { NotificationService } from 'app/services/Data Service/notification.service';
@Component({
  selector: 'app-equipes',
  templateUrl: './equipes.component.html',
  styleUrls: ['./equipes.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EquipesComponent implements OnInit {
equipe:Equipe[];
terrainfilter:Equipe[]=[];
terrainfilter2:Equipe[]=[];
terrain2:any[];
searchTerm: string;
user:boolean;
userco:string;
today: number = Date.now();
today2:any;
  constructor(private service:EquipeService,private dialog: MatDialog,private _activatedRoute: ActivatedRoute,
    private _location: Location,private datePipe: DatePipe,   
    private dialogService: DialogService,
    public notificationService: NotificationService,
    public auth:AuthService,public router: Router


    ) {     
      this.user=false;
        this.today2= datePipe.transform(this.today,'yyyy-MM-dd');
       this.searchTerm = '';
      this.service.getequipes().subscribe(equipe => {
        this.equipe = equipe.map(item => {
         
          return {
            $key: item.key,
            ...item.payload.val()
          }; 
        });
        for (let index of this.equipe) {
          if(index.Datedeb>=this.today2){
         const element = index;
         this.terrainfilter.push(element);
       }  
       }
       this.terrainfilter2=this.terrainfilter;
        this.terrain2=this.terrainfilter;
        this.terrainfilter = [];
     });
     }
     filterCoursesByTerm(): void
     {
         const searchTerm = this.searchTerm.toLowerCase(); 
 
         // Search
         if ( searchTerm === '' )
         { 
          this.terrainfilter2 = this.terrain2;
        }
         else
         {
             this.terrainfilter2 = this.terrain2.filter((terrain) => {
                 return terrain.city.toLowerCase().includes(searchTerm);
             });
         }
     }
  ngOnInit() { 
    this.auth.user$.subscribe((res)=>
    {   this.userco=res.displayName; 
    });
  }
  onEdit(row){
    this.service.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "30%";
    this.dialog.open(AddEquipeComponent,dialogConfig);
  }
  onDelete($key){
    this.dialogService.openConfirmDialog('voulez vous supprimer cette équipe ?')
    .afterClosed().subscribe(res =>{
      if(res){
        this.service.deleteequipe($key);
        this.notificationService.warn('Deleted successfully');
      }
    });
  }
   
    

  onCreate() {
    this.service.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "50%";
    this.dialog.open(AddEquipeComponent,dialogConfig);
  }
}
