import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { EquipeService } from 'app/services/Data Service/equipe.service';
import { MatDialogRef } from '@angular/material';
import { AuthService } from 'app/services/Data Service/auth.service';
import { fuseAnimations } from '@fuse/animations';
import { NotificationService } from 'app/services/Data Service/notification.service';

@Component({
  selector: 'app-add-equipe',
  templateUrl: './add-equipe.component.html',
  styleUrls: ['./add-equipe.component.scss'],
  encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddEquipeComponent implements OnInit {
name:string;
tof:string;
today: number = Date.now();

   /**
     * Constructor
     *
     * @param {MatDialogRef<AddEquipeComponent>} dialogRef
     */
  constructor(private service: EquipeService,
    public dialogRef: MatDialogRef<AddEquipeComponent>,
    private notificationService: NotificationService,
    public auth:AuthService,) { }

  ngOnInit() {
             this.auth.user$.subscribe((res)=>
       {
             this.name = res.displayName;
      if (res.photoURL === null){
                      this.tof ="https://img.icons8.com/color/100/000000/gender-neutral-user.png";
                                }
      else this.tof= res.photoURL;   
       });
   this.service.getequipes();

             }
  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.onClose();

            }
  onSubmit() {
    if (this.service.form.valid) {
      if (!this.service.form.get('$key').value)

           {
  
                  this.service.form.patchValue({capitainename: this.name});
                  this.service.form.patchValue({capitainetof: this.tof});
                  this.service.insertequipe(this.service.form.value);
           }     
            else
                  this.service.updateequipe(this.service.form.value);
                  this.service.form.reset();
                  this.service.initializeFormGroup();
                  this.notificationService.success('Submitted successfully');
                  this.onClose();
                                 } 
              }

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
            }
}
 