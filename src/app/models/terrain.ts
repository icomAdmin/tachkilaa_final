import { FuseUtils } from '@fuse/utils';

export class Terrain { 
    $key: string;
   longitude:string;
   latitude:string;
    address:string;
    nombredejoueur:number;
    name:string;
    city:string;
    typeterrain: string;
    description:string;
    userId?:String;
    prixPersone:number;
    contact:number;   
    url:string;
    // file: File;
    usermail:string;
  // constructor(file:File) {
  //   this.file = file;
  // }
}

