import { DatePipe } from '@angular/common';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { EquipeService } from 'app/services/Data Service/equipe.service';


export class Equipe { 
    $key?: string;
    city:string;
    capitainename:string;
    capitainetof:string;
    Datedeb:DatePipe;
time:string;
nombre:number;
description:string;
}

@Injectable()
export class ServerResolver implements Resolve<Equipe> {
  constructor(private serversService: EquipeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Equipe> | Promise<Equipe> | Equipe {
    return this.serversService.getequipe(+route.params['id']);
  }
}
