import { Injectable } from '@angular/core';
import 'rxjs/Rx';  
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as _ from 'lodash';
import { Http } from '@angular/http';
import { environment } from './../../../environments/environment';
import { AuthService } from './auth.service';
import { Match } from 'app/models/match';
import { TerrainDataService } from './terrain.service';

@Injectable({
  providedIn: 'root'
})
export class MatchDataService {

  url = environment.apiurl ; 
  user:string;
  matchfiltrer:Match[]=[];
  today: number = Date.now();
  today2:any;
  constructor(private firebase: AngularFireDatabase,     public auth:AuthService,

    private datePipe: DatePipe,private http: Http  ,  private terrainDataService: TerrainDataService,
    )
   { }

  matchList: AngularFireList<any>;

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    terrain: new FormControl(0),
    capitainename: new FormControl(''),
    capitainetof:new FormControl(''),
    Datedeb: new FormControl('', Validators.required),
    time: new FormControl(0, Validators.required),
  }); 

  initializeFormGroup() {
    this.form.reset(
    );
  } 
  getmatchs() {
    this.matchList = this.firebase.list('matchs');    
    return this.matchList.snapshotChanges();
    
  }
  insertmatch(match) {
    this.matchList.push({
      terrain:match.terrain,
      capitainename:match.capitainename,
      capitainetof:match.capitainetof,
      Datedeb: match.Datedeb == "" ? "" : this.datePipe.transform(match.Datedeb, 'yyyy-MM-dd'),
      time :match.time,
    });
  }
  getMatch(id){
    return this.http.get("https://tachkila-b22f2-default-rtdb.firebaseio.com/matchs/"+id+".json")
    .map(response => response.json())
}



  updatematch(match) {
    this.matchList.update(match.$key,
      {
      terrain:match.terrain,
      capitainename:match.capitainename,
      capitainetof:match.capitainetof,
      Datedeb:  this.datePipe.transform(match.Datedeb, 'yyyy-MM-dd'),
      time :match.time,
      });
  }

  deletematch($key: string) {
    this.matchList.remove($key);
  }
  populateForm(match) {
    this.form.setValue(_.omit(match,'terrainname'));
  }

}
