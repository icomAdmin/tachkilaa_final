import { Injectable } from '@angular/core';
import 'rxjs/Rx';  
import { Http } from '@angular/http';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class TerrainDataService {
  terrainList: AngularFireList<any>;
  array = [];  
  type:string="all";
  currentterrain:string;
  constructor(private firebase: AngularFireDatabase, private datePipe: DatePipe,private http: Http,
    ) {
      this.terrainList = this.firebase.list('terrain');
      this.terrainList.snapshotChanges().subscribe(
        list => {
          this.array = list.map(item => {
            return {
              $key: item.key,
              ...item.payload.val()
            };
          });
        });
   }
   form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl('', Validators.required),
    longitude: new FormControl(0, Validators.required),
    latitude: new FormControl(0, Validators.required),
    typeterrain: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
     description: new FormControl('', Validators.required),
     prixPersone: new FormControl('', Validators.required),
     contact: new FormControl('', Validators.required),
     nombredejoueur: new FormControl('', Validators.required),
     address: new FormControl('', Validators.required),
     durre:new FormControl('', Validators.required),
     usermail:new FormControl('')

  });

   getTerrainName($key) {
    if ($key == "0")
      return "";
    else{
      return _.find(this.array, (obj) => { return obj.$key == $key; })['name'];
    }
  }
    getTerraincontact($key) {
    if ($key == "0")
      return "";
    else{
      return _.find(this.array, (obj) => { return obj.$key == $key; })['contact'];
    }
  }
  

  initializeFormGroup() {
    this.form.reset(
    );
  }
  getterrains() {
    this.terrainList = this.firebase.list('terrain');
    return this.terrainList.snapshotChanges();
  }
  insertterrain(terrain) {
    this.terrainList.push({
      name: terrain.name,
      typeterrain: terrain.typeterrain,
      city: terrain.city,
      description: terrain.description,
      prixPersone: terrain.prixPersone,
      contact: terrain.contact,
      nombredejoueur: terrain.nombredejoueur,
      address: terrain.address,
      longitude: terrain.longitude,
      latitude: terrain.latitude,
      durre:terrain.durre,
      usermail:terrain.usermail

    });
  }


  updateterrain(terrain) {
    this.terrainList.update(terrain.$key,
      {
        name: terrain.name,
        typeterrain: terrain.typeterrain,
        city: terrain.city,
        description: terrain.description,
        prixPersone: terrain.prixPersone,
        contact: terrain.contact,
        nombredejoueur: terrain.nombredejoueur,
        address: terrain.address,
        longitude: terrain.longitude,
        latitude: terrain.latitude,
        durre:terrain.durre,
        usermail:terrain.usermail
      });
  }

  deleteterrain($key: string) {
    this.terrainList.remove($key);
  } 
getTerrain(id){
    return this.http.get("https://tachkila-b22f2-default-rtdb.firebaseio.com/terrain/"+id+".json")
    .map(response => response.json())
}
populateForm(terrain) {
  this.form.setValue(_.omit(terrain,'equipename'));
}


}

