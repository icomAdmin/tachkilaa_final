import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { Validators, FormGroup, FormControl } from '@angular/forms';
import { User } from 'app/models/user.model';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private firestore: AngularFirestore, public datePipe:DatePipe) { }
    userList: AngularFirestoreCollection<User>
  items: Observable<User[]>;
  form: FormGroup = new FormGroup({
    uid: new FormControl(null),
    displayName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    photoURL: new FormControl(''),
    emailVerified: new FormControl(''),

  });
  initializeFormGroup() {
    this.form.setValue({
      uid: null,
      displayName: '',
      email: '',
      photoURL: null,
      emailVerified: false,
    })
  };

  getUsers() {
    this.userList = this.firestore.collection<User>("users");
    this.items = this.userList.valueChanges();
    return this.items
  }

  getUser($key) {

    const data = this.firestore.collection<User>("users").doc($key).valueChanges();
    return data;
  }


  insertUser(user) {
    return new Promise<any>((resolve, reject) => {
      const id = this.firestore.createId();
      user.uid = id;
      const ref = this.firestore
        .collection("users")
        .doc(id)
        .set(user);


    });
  }

  updateUser(user) {
    return this.firestore
      .collection("users")
      .doc(user.uid)
      .set(user, { merge: true });
  }

  deleteUser($key: string) {
    return this.firestore
      .collection("users")
      .doc($key)
      .delete();
  }

  populateForm(user) {
    this.form.setValue(user);
  }


  getUsersNumbersByYear(year:string) {
 var resStat:Number[]=[]
 resStat['Jan']=0;resStat['Feb']=0;resStat['Mar']=0;resStat['Apr']=0;resStat['May']=0;
 resStat['Jun']=0;resStat['Jul']=0;resStat['Aug']=0;resStat['Sep']=0;resStat['Oct']=0;
 resStat['Nov']=0;resStat['Dec']=0;
 let  data:Number[]
 return new Promise(resolve=>{
         this.getUsers().subscribe(
         list => {
        for (var user of list) {  
       const userMonth =this.datePipe.transform(user.creationTime,'MMM');
       const userYear=this.datePipe.transform(user.creationTime,'yyyy');
          
       if(year==userYear)
          {
            resStat[userMonth]++;   }

      }
      
      
      resolve(resStat);  })
    }
    )
    }
}
