import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, 
  AngularFirestoreDocument 
} from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';


import 'rxjs/Rx';  
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { User } from '../../models/user.model';

@Injectable({providedIn:'root'})
export class AuthService {

  user$: Observable<User>;

  name: string;
  typeLogin: string = "";
  verif: boolean;

    constructor(
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router,
        private _fuseNavigationService: FuseNavigationService,
        public ngZone: NgZone,

    ) {
      this.user$ = this.afAuth.authState.pipe(
        switchMap(user => {
          if (user) {
            const ResUser = this.afs.doc<User>(`users/${user.uid}`).valueChanges();
            ResUser.subscribe(res => {
              if (res.type == 'admin') {
                this._fuseNavigationService.updateNavigationItem('admin-options', {
                  hidden: false
                })
  
                
              } else {
                this._fuseNavigationService.updateNavigationItem('admin-options', {
                  hidden: true
                })
  
                   }
            } )
            return ResUser;
          }
            else {
                return of(null);
              }
        })
      );
     }
 SignUp(email, password, name: string) {
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password).
        then((result) => {
  
          result.user.updateProfile({
            displayName: name
          }).then(() => {
            this.SendVerificationMail();
            this.updateUserData(result.user);
          })
        }
        ).catch((error) => {
          window.alert(error.message)
        }
        )
    }
ForgotPassword(passwordResetEmail) {
      return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
        .then(() => {
          window.alert('Password reset email sent, check your inbox.');
        }).catch((error) => {
          window.alert(error)
        })
    }
    async SignIn(email, password) {
      return await this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then(async (result) => {
          this.typeLogin = "emailPass";
          await this.updateUserData(result.user);
          this.ngZone.run(async () => {
            await this.router.navigate(['/dashbord']);
          });
        }).catch((error) => {
          window.alert(error.message)
        })
    }

    async googleSignin() {
      const provider = new auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      this.typeLogin = "google";
      await this.updateUserData(credential.user);
  
      return this.router.navigate(['/dashbord']);
    }
    async facebookSignin() {
      const provider = new auth.FacebookAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      this.typeLogin = "facebook";
      await this.updateUserData(credential.user);
  
      return this.router.navigate(['/dashbord']);
    }

    async signOut() {
      await this.afAuth.auth.signOut();
      return await this.router.navigate(['/login']);
    }
   
    SendVerificationMail() {
      return this.afAuth.auth.currentUser.sendEmailVerification()
        .then(() => {
          this.router.navigate(['mail-confirm']);
        })
    }
    
    private updateUserData(user) {
      // Sets user data to firestore on login
      const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
      let verif: boolean;
  
  
      (this.typeLogin == "facebook") ? verif = true : verif = user.emailVerified;
  
      const data = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
        emailVerified: verif,
        lastSignInTime: user.metadata.lastSignInTime,
        creationTime: user.metadata.creationTime,
        typeAuth: this.typeLogin,
      };
      console.log(data);
      return userRef.set(data, { merge: true });
    }
    get authUser() {
      let user: User;
  
      user = {
        uid: this.afAuth.auth.currentUser.uid, email: this.afAuth.auth.currentUser.email
        , displayName: this.afAuth.auth.currentUser.displayName
      };
      return user;
    }
  
    get Logintype(): string {
  
      return this.typeLogin;
    }
    public updateUsertype(type: string) {
      // Sets user type to firestore on login
      const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${this.afAuth.auth.currentUser.uid}`);
  
  
      const data = {
        uid: this.afAuth.auth.currentUser.uid,
        type: type,
        validated: true,
  
      };
      console.log(data);
      return userRef.set(data, { merge: true });
    }
    public addUserInfo(userInfo) {
      const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${this.afAuth.auth.currentUser.uid}`);
      console.log(userInfo.value.adresse);
  
      const data = {
        uid: this.afAuth.auth.currentUser.uid,
  
        raisonSociale: userInfo.value.raisonSociale,
        adresse: userInfo.value.adresse,
        telephone: userInfo.value.telephone,
        region: userInfo.value.region,
        status: userInfo.value.status,
        numFiscal: userInfo.value.numFiscal,
        tarif: userInfo.value.tarif,
        description: userInfo.value.description,
        domaine: userInfo.value.domaine,
  
  
        type: "prestataire",
        validated: true,
  
      };
      console.log(data);
      //this.afAuth.auth.currentUser.reload()
      return userRef.set(data, { merge: true });
    }  
}
 