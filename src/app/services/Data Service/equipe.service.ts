import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { DatePipe } from '@angular/common';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  constructor(private firebase: AngularFireDatabase, private datePipe: DatePipe,private http: Http)
  { }

 equipeList: AngularFireList<any>;
 
 form: FormGroup = new FormGroup({
   $key: new FormControl(null),
   city: new FormControl('', Validators.required),
   capitainename: new FormControl(''),
   capitainetof:new FormControl(''),
   Datedeb: new FormControl('', Validators.required),
   time: new FormControl(0, Validators.required),
   nombre:new FormControl(0, Validators.required),
   description:new FormControl('',Validators.required),
 }); 

 initializeFormGroup() {
   this.form.reset(
   );
 } 
 getequipes() {
   this.equipeList = this.firebase.list('equipes');
   return this.equipeList.snapshotChanges();
 }


 insertequipe(equipe) {
   this.equipeList.push({
     city:equipe.city,
     capitainename:equipe.capitainename,
     capitainetof:equipe.capitainetof,
     Datedeb: equipe.Datedeb == "" ? "" : this.datePipe.transform(equipe.Datedeb, 'yyyy-MM-dd'),
     time :equipe.time,
     nombre :equipe.nombre,
     description :equipe.description,
   });
 }
 getequipe(id){
   return this.http.get("https://tachkila-b22f2-default-rtdb.firebaseio.com/equipes/"+id+".json")
   .map(response => response.json())
}

 updateequipe(equipe) {
   this.equipeList.update(equipe.$key,
     {
     city:equipe.city,
     capitainename:equipe.capitainename,
     capitainetof:equipe.capitainetof,
     Datedeb: equipe.Datedeb == "" ? "" : this.datePipe.transform(equipe.Datedeb, 'yyyy-MM-dd'),
     time :equipe.time,
     nombre :equipe.nombre,
     description :equipe.description,
     });
 }

 deleteequipe($key: string) {
   this.equipeList.remove($key);
 }
 populateForm(equipe) {
   this.form.setValue(_.omit(equipe,'terrainname'));
 }

}
 