import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { AuthService } from '../Data Service/auth.service';

@Injectable()
export class NotAuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, ) {}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {

      return this.auth.user$
           .take(1)
           .map(user => !!(user && user.emailVerified==true) )
           .do(loggedIn => {
             if (loggedIn) {
               this.router.navigate(['/dashbord']);
             }
         })

  }
}