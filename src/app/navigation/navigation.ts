//import { FuseNavigation } from '@fuse/types/fuse-navigation';
import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'Menu',
        title    : 'Menu',
        type     : 'group',

         children : [
            {
                id       : 'dashbord',
                title    : 'dashbord',
                type     : 'item',
                url      : '/dashbord',
                icon :'home',

            },
            {
                id       : 'Resérvation',
                title    : 'reservation',
                type     : 'item',
                url      : '/reservation',
                icon:'date_range',
                badge    : {
                    title    : '25',
                    // translate: 'NAV.MAIL.BADGE',
                    translate: '11',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
                

            },
            {
                id       : 'Les matchs',
                title    : 'Les matchs',
                type     : 'item',
                url      : '/listematch',
                icon:'group',
                

             },
            //  {
            //     id       : 'Devenir Sponseur',
            //     title    : 'Devenir Sponseur',
            //     translate: 'NAV.SAMPLE.TITLE',
            //     type     : 'item',
            //     url      : '/sponseur',
            //     icon:'how_to_reg',

            //  },
             {
                id       : 'Les Terrains',
                title    : 'Les Terrains',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'collapsable',
                url      : '/listeterrain',
                icon :'filter_tilt_shift',
                children : [
                    {
                        id       : 'Foot Ball',
                        title    : 'Foot Ball',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/foot',
        
                    },
                    {
                        id       : 'Basket Ball',
                        title    : 'Basket Ball',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/basket',
        
                    }, {
                        id       : 'Hand Ball',
                        title    : 'Hand Ball',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/hand',
    
                    }, {
                        id       : 'Volley Ball',
                        title    : 'Volley Ball',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/volley',
        
                    }, {
                        id       : 'Tennis',
                        title    : 'Tennis',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/tennis',
        
                    }, {
                        id       : 'Cource',
                        title    : 'Cource',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        url      : '/course',
        
                    }
                ]
            }
        ]
    }
];