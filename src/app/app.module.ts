import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { fuseConfig } from 'app/fuse-config';
import { FakeDbService } from 'app/fake-db/fake-db.service';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-matrial.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';

const config = {
    apiKey: "AIzaSyCV8AJ8tNvwy9v45FnPEIVfIb8A0dRnNWY",
    authDomain: "tachkila-b22f2.firebaseapp.com",
    databaseURL: "https://tachkila-b22f2-default-rtdb.firebaseio.com",
    projectId: "tachkila-b22f2",
    storageBucket: "tachkila-b22f2.appspot.com",
    messagingSenderId: "175709367771",
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        AngularFireModule.initializeApp(config),
        AngularFirestoreModule,
        AngularFireAuthModule,
        TranslateModule.forRoot(),
        AngularFireDatabaseModule,
       

        // Material moment date module

        MatMomentDateModule,

        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule, 
        AngularFireModule.initializeApp(config),
        AngularFirestoreModule,
        AngularFireAuthModule,
        TranslateModule.forRoot(),
        AngularFireStorageModule,      
        // Material
        AppMaterialModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        FuseSidebarModule,

        // App modules
        LayoutModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        AngularFireDatabaseModule,       
        AppRoutingModule, 
        AppMaterialModule,
        LayoutModule,
        NgxChartsModule,
    ],
    bootstrap   : [
        AppComponent
    ],
    schemas:  [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class AppModule
{
}
