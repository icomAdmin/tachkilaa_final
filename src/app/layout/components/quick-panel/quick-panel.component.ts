import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Message, ChatService } from 'app/services/Data Service/chat.service';
import { Observable } from 'rxjs/Observable';
    import 'rxjs/add/operator/scan';
import { Subject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

@Component({
    selector     : 'quick-panel',
    templateUrl  : './quick-panel.component.html',
    styleUrls    : ['./quick-panel.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuickPanelComponent implements OnInit, OnDestroy
{
    date: Date;
    events: any[];
    notes: any[];
    settings: any;
    messages: Observable<Message[]>;
    formValue: string;
    private _replyInput: ElementRef;
    private _replyForm: NgForm;

    @ViewChild('replyForm')
    set replyForm(content: NgForm)
    {
        this._replyForm = content;
    }

    @ViewChild('replyInput')
    set replyInput(content: ElementRef)
    {
        this._replyInput = content;
    }

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param {FuseSidebarService} _fuseSidebarService
     */
    

    constructor(
        private _httpClient: HttpClient,
        public chat: ChatService,
        private _fuseSidebarService: FuseSidebarService
    )
    {
       

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
         // appends to array after each new message is added to feedSource
    this.messages = this.chat.conversation.asObservable()
    .scan((acc, val) => acc.concat(val) );
            
    }
    sendMessage() {
        this.chat.converse(this.formValue);
        this.formValue = '';
      }
      foldSidebarTemporarily(): void
      {
          this._fuseSidebarService.getSidebar('chatPanel').foldTemporarily();
      }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
