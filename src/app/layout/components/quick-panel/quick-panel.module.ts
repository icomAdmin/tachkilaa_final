import { NgModule } from '@angular/core';
import { MatDividerModule, MatListModule, MatSlideToggleModule, MatIconModule, MatFormFieldModule, MatTooltipModule, MatTextareaAutosize, MatTableModule, MatInputModule, MatButtonModule, MatTabsModule, MatRippleModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { QuickPanelComponent } from 'app/layout/components/quick-panel/quick-panel.component';


@NgModule({
    declarations: [
        QuickPanelComponent
    ],
    imports     : [
        MatDividerModule,
        MatListModule,
        MatSlideToggleModule,
        MatIconModule,
        FuseSharedModule,
        MatFormFieldModule,
        MatTooltipModule,
        MatInputModule , 
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        MatTooltipModule,
        MatRippleModule,
       
        FuseSharedModule
    ],
    exports: [
        QuickPanelComponent
    ]
})
export class QuickPanelModule
{
}
